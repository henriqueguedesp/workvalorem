

function buscaEndereco() {
    var cep = $('#cep').val();
    var aux = cep.replace(/-/g, "");


    var link = "https://viacep.com.br/ws/" + cep + "/json/";
    $.ajax({
        url: link,
        type: 'GET',
        dataType: 'json',
        success: function (dado) {

            document.getElementById('logradouro').value = dado.logradouro;
            document.getElementById('complemento').value = dado.complemento;
            document.getElementById('bairro').value = dado.bairro;
            document.getElementById('cidade').value = dado.localidade;
            document.getElementById('estado').value = dado.uf;

        },
        error: function () {
        }
    });
}

//função de cadastro
$(document).ready(function () {

    $('#form').submit(function () {


        // alert('oii'); 
        const data = $('#form').serialize();
        console.log(data);
        $.ajax({//Função AJAX
            url: "enviarCurriculo",
            type: "post",
            data: new FormData('form'),
            contentType: false,
            cache: false,
            processData: false,
            success: function (result) {
                if (result == 1) {
                    jQuery.noConflict();
                    $("#cadastro").hide();
                    $("#sucessoCadastro").modal();
                } else {

                }
            },
            error: function () {
                jQuery.noConflict();
                $("#cadastro").hide();
                $("#erro").modal();
            }
        });
        return false;


    });

});
