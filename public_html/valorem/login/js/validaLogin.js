$(document).ready(function () {
    $('.login-form').submit(function () {
        var usuario = $('#usuario').val();
        var senha = $('#senha').val();
       
        if (usuario == "" || senha == "") {
            $("#output").removeClass(' alert alert-success');
            $("#output").addClass("alert alert-danger animated fadeInUp").html("CNPJ e senha não digitados!");
            document.getElementById('usuario').style.border = "2px solid #FF6347";
            document.getElementById('senha').style.border = "2px solid #FF6347";
        } else {
            $.ajax({//Função AJAX
                url: "validaLogin",
                type: "post",
                data: {usuario: usuario, senha: senha},

                success: function (result) {
                    if (result == 1) {
                        location.href = "/gestao";
                    } else  {
                        $("#output").removeClass(' alert alert-success');
                        $("#output").addClass("alert alert-warning animated fadeInUp").html("Usuário ou senha não encontrados!");
                        document.getElementById('email').style.border = "";
                        document.getElementById('senha').style.border = "";


                    } 
                },
                error: function () {
                    alert('Erro 664!');
                }
            });
        }

        return false;
    });
});
