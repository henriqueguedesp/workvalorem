<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WorkValorem\Controllers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use WorkValorem\Entity\Formulario;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Description of ControleEnvio
 *
 * @author henrique.guedes
 */
class ControleEnvio {

    function __construct() {
        
    }

    //public function enviar(Formulario $formulario, \Symfony\Component\HttpFoundation\File\UploadedFile $arquivo = null) {
    public function enviar(Formulario $formulario,  \Symfony\Component\HttpFoundation\File\UploadedFile $arquivo = null) {
        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        $mail->Host = "smtp.office365.com"; // Seu endereço de host SMTP
        $mail->SMTPAuth = true; // Define que será utilizada a autenticação -  Mantenha o valor "true"
        $mail->Port = 587; // Porta de comunicação SMTP - Mantenha o valor "587"
        $mail->SMTPSecure = true; // Define se é utilizado SSL/TLS - Mantenha o valor "false"
        $mail->SMTPAutoTLS = true; // Define se, por padrão, será utilizado TLS - Mantenha o valor "false"
        $mail->Username = 'recrutamento@ubsvalorem.com.br'; // Conta de email existente e ativa em seu domínio
        $mail->Password = 'Jal73508'; // Senha da sua conta de email
// DADOS DO REMETENTE
        $mail->Sender = "recrutamento@ubsvalorem.com.br"; // Conta de email existente e ativa em seu domínio
        $mail->From = "recrutamento@ubsvalorem.com.br"; // Sua conta de email que será remetente da mensagem
        $mail->FromName = "No Reply Valorem Agronegócios"; // Nome da conta de email
// DADOS DO DESTINATÁRIO
        $mail->AddAddress('recrutamento@ubsvalorem.com.br'); // Define qual conta de email receberá a mensagem
//$mail->AddAddress('recebe2@dominio.com.br'); // Define qual conta de email receberá a mensagem
//$mail->AddCC('copia@dominio.net'); // Define qual conta de email receberá uma cópia
//$mail->AddBCC('copiaoculta@dominio.info'); // Define qual conta de email receberá uma cópia oculta
// Definição de HTML/codificação
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)
// DEFINIÇÃO DA MENSAGEM
        date_default_timezone_set('America/Sao_Paulo');

        $hora = date('H:i');
        $cumprimento = null;
        if ((strtotime($hora) >= (strtotime('18:00'))) and ( strtotime($hora) <= (strtotime('04:59')))) {
            //BOA NOITE
            $cumprimento = 'Boa noite!';
        } else {
            if ((strtotime($hora) >= (strtotime('05:00'))) and ( strtotime($hora) <= (strtotime('11:59')))) {
                //BOM DIA
                $cumprimento = 'Bom dia!';
            } else {
                //BOA TARDE
                $cumprimento = 'Boa tarde!';
            }
        }


        $mail->Subject = "Currículo cadastrado no Trabalhe Conosco"; // Assunto da mensagem
     //   $mail->Body .= "teste,<br><br>"; // Texto da mensagem
         $mail->Body .= $cumprimento . "<br><br>"; // Texto da mensagem
          $mail->Body .= "Currículo cadastrado no Trabalhe Conosco<br><br>"; // Texto da mensagem
          $mail->Body .= "DADOS<br>"; // Texto da mensagem
          $mail->Body .= "Nome completo: " . $formulario->getNome(). "<br>"; // Texto da mensagem
          $mail->Body .= "Nacionalidade: " . $formulario->getNacionalidade() . "<br>"; // Texto da mensagem
          $mail->Body .= "Data Nascimento: " . $formulario->getDataNascimento(). "<br>"; // Texto da mensagem
          $mail->Body .= "Estado Civil: " . $formulario->getEstadoCivil() . "<br>"; // Texto da mensagem
          $mail->Body .= "Sexo: " . $formulario->getSexo(). "<br>"; // Texto da mensagem
          $mail->Body .= "Curso: " . $formulario->getCurso(). "<br>"; // Texto da mensagem
          $mail->Body .= "Instituição: " . $formulario->getInstituicao() . "<br>"; // Texto da mensagem
          $mail->Body .= "Escolaridade: " . $formulario->getEscolaridade() . "<br>"; // Texto da mensagem
          $mail->Body .= "Como soube da Valorem: " . $formulario->getSoubeValorem(). "<br>"; // Texto da mensagem
          $mail->Body .= "Está empregado: " . $formulario->getEmpregado(). "<br>"; // Texto da mensagem
          
         
        //  $mail->Body .= "Informamos que a sala <b>" . $dados->descricao . "</b> do <b>" . $dados->setor . "</b> foi reservada por <b>" . $dados->nome . "</b> para o dia <b>" . date('d-m-Y', strtotime($dados->dataInicio)) . "</b> de <b>" . $dados->horaInicio . "</b> às <b>" . $dados->horaTermino . "</b> para <b>" . $dados->para . "</b>!"; // Texto da mensagem
        //$mail->Body .= " E-mail: " . "<br>"; // Texto da mensagem
        //  $mail->Body .= " Assunto: " . "<br>"; // Texto da mensagem
        //$mail->Body .= " Mensagem: " . "<br>"; // Texto da mensagem
        //ANEXO
        // $mail->AddAttachment($arquivo->openFile());
        //    var_dump($arquivo);
        //exit;
       
        if ($arquivo) {
            /*
            $mail->AddAttachment($arquivo['tmp_name'], $arquivo['name']);
            print_r($arquivo);
            exit;*/
            $mail->AddAttachment($arquivo->getPathname(), $arquivo->getClientOriginalName());
        }

        //$mail->AddAttachment($arquivo->getClientOriginalName() );
// ENVIO DO EMAIL
        $enviado = $mail->Send();
// Limpa os destinatários e os anexos
        $mail->ClearAllRecipients();

// Exibe uma mensagem de resultado do envio (sucesso/erro)
        if ($enviado) {
            return 0;
        } else {
            return -1;
            //echo "<b>Detalhes do erro:</b> " . $mail->ErrorInfo;
        }
    }

}
