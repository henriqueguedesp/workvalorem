<?php

namespace WorkValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WorkValorem\Util\Sessao;
use WorkValorem\Entity\Formulario;
use WorkValorem\Controllers\ControleEnvio;
use WorkValorem\Models\ModeloUsuario;

class ControleIndex
{

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao)
    {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function gestao()
    {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $modelo = new ModeloUsuario();
            $dados = $modelo->listaCurriculos();
            foreach ($dados as $key => $da) {
                if($dados[$key]->idAnexo){
                    $dados[$key]->data = base64_encode(($dados[$key]->data));
                }
                
            }
            return $this->response->setContent($this->twig->render('Gestao.html.twig', array('user' => $usuario, 'curriculos' => $dados)));
        } else {
            $this->redireciona('/login');
        }
    }

    public function index()
    {
        return $this->response->setContent($this->twig->render('Index.html.twig'));
    }

    public function agradecimento()
    {
        return $this->response->setContent($this->twig->render('Agradecimento.html.twig'));
    }

    public function enviarCurriculo()
    {
        $formulario = new Formulario();
        $formulario->setNome($this->request->get('nomeCompleto'));
        $formulario->setNacionalidade($this->request->get('nacionalidade'));
        $formulario->setDataNascimento($this->request->get('dataNascimento'));

        $formulario->setEstadoCivil($this->request->get('estadoCivil'));
        $formulario->setSexo($this->request->get('sexo'));
        $formulario->setCurso($this->request->get('curso'));
        $formulario->setInstituicao($this->request->get('instituicao'));
        $formulario->setEscolaridade($this->request->get('escolaridade'));
        $formulario->setSoubeValorem($this->request->get('soubeValorem'));
        $formulario->setEmpregado($this->request->get('empregado'));
        $formulario->setInteresse($this->request->get('interesse'));
        //       $arquivo = $this->request->files->get('curriculo');
        //$arquivo = $this->request->files->get('curriculo');
        // $teste = $_FILES["curriculo"];
        $teste = $this->request->files->get('curriculo');
   
        if (!$teste) {
            $teste = null;
        }/*
print_r($teste);
exit;*/
        $controleEnvio = new ControleEnvio();
        $dado = $controleEnvio->enviar($formulario, $teste);
        $modelo = new ModeloUsuario();
        $dado = $modelo->salvaCurriculo($formulario, $teste);
        if ($dado = 1) {
            $this->redireciona('/agradecimento');
        } else {
        }
        //$controleEnvio->enviar();
        //   var_dump($this->request);
        //  var_dump($this->request->files);
        //exit;
        //     print_r($formulario);
        //  print_r($arquivo);
        //print_r($teste);
        //print_r('erro');
        // exit;
        //print_r('oppp');
        //return $this->response->setContent($this->twig->render('Index2.html.twig'));
    }

    public function redireciona($destino)
    {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }
}
