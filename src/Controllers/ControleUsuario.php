<?php

namespace WorkValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use WorkValorem\Util\Sessao;
use WorkValorem\Models\ModeloUsuario;

class ControleUsuario {

    private $response;
    private $twig;
    private $request;
    private $sessao;
    private $raiz = "/";

   
    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function login() {
        $usuario = $this->sessao->get('usuario');
        if ($usuario) {
            $this->redireciona($this->raiz);
        } else {
            return $this->response->setContent($this->twig->render('Login.html.twig'));
        }
    }

    public function validaLogin() {/*
         $modulo = 'SM';
        $usuario = $this->request->get('usuario');
        $senha = $this->request->get('senha');
        $modelo = new ModeloUsuario();
        //$retorno = $modelo->validaLogin($usuario, $senha);
        $validaUsuario = $modelo->validaUsuario($usuario);
        if ($validaUsuario) {
            $usuarioAtivo = $modelo->usuarioAtivo($usuario);
            if ($usuarioAtivo) {
                $valida = $modelo->validaSenha($usuario, $senha);
                if ($valida) {
                    $moduloAtivo = $modelo->verificaAtivado($modulo);
                    if ($moduloAtivo) {

                        $acesso = $modelo->validaLogin($usuario, $senha);
                        if ($acesso) {
                            $this->sessao->add("usuario", $acesso);
                            echo 1;
                        } else {
                            echo 14;
                        }
                    } else {
                        echo 13;
                    }
                } else {
                    echo 12;
                }
            } else {
                echo 11;
            }
        } else {
            echo 10;
        }*/
        $usuario = $this->request->get('usuario');
        $senha = $this->request->get('senha');
        $modelo = new ModeloUsuario();
        $retorno = $modelo->validaLogin($usuario, $senha);

        if ($retorno) {
            $this->sessao->add("usuario", $retorno);
            echo 1;
        } else {
            echo 0;
        }

    }

    public function removerUsuario() {
        if ($this->sessao->get("usuario")) {
            $this->sessao->remove('usuario');
            $this->sessao->delete('usuario');
            $this->redireciona($this->raiz);
        } else {
            $this->redireciona($this->raiz."login");
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
