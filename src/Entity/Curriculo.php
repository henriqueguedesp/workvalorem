<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WorkValorem\Entity;


class Curriculo
{

    private $idCurriculo;
    private $idAnexo;
    private $nomeCompleto;
    private $nacionalidade;
    private $dataNascimento;
    private $estadoCivil;
    private $sexo;
    private $curso;
    private $instituicao;
    private $escolaridade;
    private $areaInteresse;
    private $comoSoube;
    private $empregado;

    function __construct()
    {
    }

    


    /**
     * Get the value of empregado
     */ 
    public function getEmpregado()
    {
        return $this->empregado;
    }

    /**
     * Set the value of empregado
     *
     * @return  self
     */ 
    public function setEmpregado($empregado)
    {
        $this->empregado = $empregado;

        return $this;
    }

    /**
     * Get the value of comoSoube
     */ 
    public function getComoSoube()
    {
        return $this->comoSoube;
    }

    /**
     * Set the value of comoSoube
     *
     * @return  self
     */ 
    public function setComoSoube($comoSoube)
    {
        $this->comoSoube = $comoSoube;

        return $this;
    }

    /**
     * Get the value of areaInteresse
     */ 
    public function getAreaInteresse()
    {
        return $this->areaInteresse;
    }

    /**
     * Set the value of areaInteresse
     *
     * @return  self
     */ 
    public function setAreaInteresse($areaInteresse)
    {
        $this->areaInteresse = $areaInteresse;

        return $this;
    }

    /**
     * Get the value of escolaridade
     */ 
    public function getEscolaridade()
    {
        return $this->escolaridade;
    }

    /**
     * Set the value of escolaridade
     *
     * @return  self
     */ 
    public function setEscolaridade($escolaridade)
    {
        $this->escolaridade = $escolaridade;

        return $this;
    }

    /**
     * Get the value of instituicao
     */ 
    public function getInstituicao()
    {
        return $this->instituicao;
    }

    /**
     * Set the value of instituicao
     *
     * @return  self
     */ 
    public function setInstituicao($instituicao)
    {
        $this->instituicao = $instituicao;

        return $this;
    }

    /**
     * Get the value of idCurriculo
     */ 
    public function getIdCurriculo()
    {
        return $this->idCurriculo;
    }

    /**
     * Set the value of idCurriculo
     *
     * @return  self
     */ 
    public function setIdCurriculo($idCurriculo)
    {
        $this->idCurriculo = $idCurriculo;

        return $this;
    }

    /**
     * Get the value of idAnexo
     */ 
    public function getIdAnexo()
    {
        return $this->idAnexo;
    }

    /**
     * Set the value of idAnexo
     *
     * @return  self
     */ 
    public function setIdAnexo($idAnexo)
    {
        $this->idAnexo = $idAnexo;

        return $this;
    }

    /**
     * Get the value of nomeCompleto
     */ 
    public function getNomeCompleto()
    {
        return $this->nomeCompleto;
    }

    /**
     * Set the value of nomeCompleto
     *
     * @return  self
     */ 
    public function setNomeCompleto($nomeCompleto)
    {
        $this->nomeCompleto = $nomeCompleto;

        return $this;
    }

    /**
     * Get the value of nacionalidade
     */ 
    public function getNacionalidade()
    {
        return $this->nacionalidade;
    }

    /**
     * Set the value of nacionalidade
     *
     * @return  self
     */ 
    public function setNacionalidade($nacionalidade)
    {
        $this->nacionalidade = $nacionalidade;

        return $this;
    }

    /**
     * Get the value of dataNascimento
     */ 
    public function getDataNascimento()
    {
        return $this->dataNascimento;
    }

    /**
     * Set the value of dataNascimento
     *
     * @return  self
     */ 
    public function setDataNascimento($dataNascimento)
    {
        $this->dataNascimento = $dataNascimento;

        return $this;
    }

    /**
     * Get the value of estadoCivil
     */ 
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set the value of estadoCivil
     *
     * @return  self
     */ 
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    /**
     * Get the value of sexo
     */ 
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set the value of sexo
     *
     * @return  self
     */ 
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get the value of curso
     */ 
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * Set the value of curso
     *
     * @return  self
     */ 
    public function setCurso($curso)
    {
        $this->curso = $curso;

        return $this;
    }
}
