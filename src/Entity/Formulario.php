<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WorkValorem\Entity;

/**
 * Description of Formulario
 *
 * @author henrique.guedes
 */
class Formulario {
    private $nome;
    private $nacionalidade;
    private $dataNascimento;
    private $curso;
    private $instituicao;
    private $estadoCivil;
    private $sexo;
    private $soubeValorem;
    private $empregado;
    private $escolaridade; 
    private $arquivo;
    private $interesse;
    
    function __construct() {
        
    }
    
    function getArquivo() {
        return $this->arquivo;
    }

    function setArquivo($arquivo) {
        $this->arquivo = $arquivo;
    }

        
    function getNome() {
        return $this->nome;
    }

    function getNacionalidade() {
        return $this->nacionalidade;
    }

    function getDataNascimento() {
        return $this->dataNascimento;
    }

    function getCurso() {
        return $this->curso;
    }

    function getInstituicao() {
        return $this->instituicao;
    }

    function getEstadoCivil() {
        return $this->estadoCivil;
    }

    function getSexo() {
        return $this->sexo;
    }

    function getSoubeValorem() {
        return $this->soubeValorem;
    }

    function getEmpregado() {
        return $this->empregado;
    }

    function getEscolaridade() {
        return $this->escolaridade;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setNacionalidade($nacionalidade) {
        $this->nacionalidade = $nacionalidade;
    }

    function setDataNascimento($dataNascimento) {
        $this->dataNascimento = $dataNascimento;
    }

    function setCurso($curso) {
        $this->curso = $curso;
    }

    function setInstituicao($instituicao) {
        $this->instituicao = $instituicao;
    }

    function setEstadoCivil($estadoCivil) {
        $this->estadoCivil = $estadoCivil;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function setSoubeValorem($soubeValorem) {
        $this->soubeValorem = $soubeValorem;
    }

    function setEmpregado($empregado) {
        $this->empregado = $empregado;
    }

    function setEscolaridade($escolaridade) {
        $this->escolaridade = $escolaridade;
    }



    

    /**
     * Get the value of interesse
     */ 
    public function getInteresse()
    {
        return $this->interesse;
    }

    /**
     * Set the value of interesse
     *
     * @return  self
     */ 
    public function setInteresse($interesse)
    {
        $this->interesse = $interesse;

        return $this;
    }
}
