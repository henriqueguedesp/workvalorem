<?php

namespace WorkValorem\Entity;


class Usuario {

    private $idUsuario;
    private $nome;
    private $senha;
    private $email;
    private $tipo;
    
    function getIdUsuario() {
        return $this->idUsuario;
    }
    function getTipo() {
        return $this->tipo;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

        function getNome() {
        return $this->nome;
    }


    function getSenha() {
        return $this->senha;
    }


    function getEmail() {
        return $this->email;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }


    function setSenha($senha) {
        $this->senha = $senha;
    }


    function setEmail($email) {
        $this->email = $email;
    }

        
    function __construct() {
        
    }

}
