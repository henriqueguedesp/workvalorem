<?php

namespace WorkValorem\Routes;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$rotas = new RouteCollection();

$rotas->add('raiz', new Route('/', array(
    '_controller' =>
    'WorkValorem\Controllers\ControleIndex',
    '_method' => 'index'
)));

$rotas->add('gestao', new Route('/gestao', array(
    '_controller' =>
    'WorkValorem\Controllers\ControleIndex',
    '_method' => 'gestao'
)));

$rotas->add('enviarCurriculo', new Route('/enviarCurriculo', array(
    '_controller' =>
    'WorkValorem\Controllers\ControleIndex',
    '_method' => 'enviarCurriculo'
)));

$rotas->add('agradecimento', new Route('/agradecimento', array(
    '_controller' =>
    'WorkValorem\Controllers\ControleIndex',
    '_method' => 'agradecimento'
)));

##LOGIN
$rotas->add('login', new Route('/login', array(
    '_controller' =>
    'WorkValorem\Controllers\ControleUsuario',
    '_method' => 'login'
)));

$rotas->add('validaLogin', new Route('/validaLogin', array(
    '_controller' =>
    'WorkValorem\Controllers\ControleUsuario',
    '_method' => 'validaLogin'
)));
$rotas->add('removerUsuario', new Route('/removerUsuario', array(
    '_controller' =>
    'WorkValorem\Controllers\ControleUsuario',
    '_method' => 'removerUsuario'
)));

return $rotas;
